using System;
using System.Threading.Tasks;
using MongoDB.Driver;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepository
        : ICustomerRepository
    {
        const string connectionString = "mongodb://127.0.0.1:27017";//"mongodb+srv://user:user@cluster0.qcuhp.mongodb.net/TestOtus?retryWrites=true&w=majority";
        IMongoCollection<Customer> _collection;

        public CustomerRepository()
        {
            var client = new MongoClient(connectionString);
            var database = client.GetDatabase("TestOtus");
            database.DropCollection("Customers");
            database.CreateCollection("Customers");
            _collection = database.GetCollection<Customer>("Customers");
        }

        public IMongoCollection<Customer> GetCollection()
        {
            var client = new MongoClient(connectionString);
            var database = client.GetDatabase("TestOtus");
            var collection = database.GetCollection<Customer>("Customers");
            return collection;
        }

        public async Task<Customer> GetCustomerAsync(string id)
        {
            // ��������� ��������
            var builder = new FilterDefinitionBuilder<Customer>();
            var filter = builder.Empty; // ������ ��� ������� ���� ����������
            // ������ �� id

            if (!string.IsNullOrWhiteSpace(id))
            {
                filter = filter & builder.Eq("Id", id);
            }

            return await _collection.Find(filter).FirstOrDefaultAsync();
        }

        public async Task AddCustomerAsync(Customer customer)
        {
            await _collection.InsertOneAsync(customer);
        }
    }
}