﻿using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class CSVParser : IDataParser<List<Customer>>
    {
        private readonly string _fileName;
        public CSVParser(string fileName)
        {
            _fileName = fileName;
        }

        public List<Customer> Parse()
        {
            var customerList = new List<Customer>();
            using (var fs = new FileStream(_fileName, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                using (var reader = new StreamReader(fs))
                {
                    while (!reader.EndOfStream)
                    {
                        var e = reader.ReadLine();
                        var customerData = e.Split(",");
                        var customer = new Customer()
                        {
                            Id = int.Parse(customerData[0]),
                            Email = customerData[1],
                            Phone = customerData[2],
                            FullName = customerData[3]
                        };
                        customerList.Add(customer);
                    }
                }
            }
            return customerList;
        }
    }
}
