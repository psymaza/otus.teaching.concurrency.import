﻿using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using System;
using System.Net.Http;
using System.Text;
using System.Text.Json;

namespace ConsoleClient
{
    class Program
    {
        static void Main(string[] args)
        {
            var customer = RandomCustomerGenerator.Generate(1)[0];
            var client = new HttpClient();
            var content = new StringContent(JsonSerializer.Serialize(customer, customer.GetType()), Encoding.UTF8, "application/json");
            var response = client.PostAsync("https://localhost:44364/api/customers", content).GetAwaiter().GetResult();
        }
    }
}
