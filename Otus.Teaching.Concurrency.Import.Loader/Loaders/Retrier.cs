﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    static class Retrier
    {
        public static bool Retry(Action action, int maxTries)
        {
            var numberOfRetries = 0;
            while (maxTries > numberOfRetries)
            {
                try
                {
                    action.Invoke();
                    return true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Ошибка добавления записи в базу {ex.Message}. Попытка {numberOfRetries}");
                    numberOfRetries++;
                }
            }
            return false;
        }
    }
}
