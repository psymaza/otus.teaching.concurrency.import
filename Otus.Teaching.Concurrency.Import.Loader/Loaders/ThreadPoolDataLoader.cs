﻿using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public class ThreadPoolDataLoader : IDataLoader
    {
        private List<Customer> _customers;
        private int _maxNumberOfRetries;
        private  ICustomerRepository _repository;
        private CountdownEvent _countdownEvent;
        private int _numberOfThreads;

        public ThreadPoolDataLoader(List<Customer> customers, int maxNumberOfRetries, ICustomerRepository repository, int numberOfThreads)
        {
            _customers = customers;
            _maxNumberOfRetries = maxNumberOfRetries;
            _repository = repository;
            _numberOfThreads = numberOfThreads;
        }
        public void LoadData()
        {

            Console.WriteLine("Loading data...");
            var sw = new Stopwatch();
            sw.Start();
            using (_countdownEvent = new CountdownEvent(_numberOfThreads))
            {
                for (int i = 0; i < _numberOfThreads; i++)
                {
                    var customerChunkParameters = new CustomerChunkParameters();
                    customerChunkParameters.ChunkSize = _customers.Count / _numberOfThreads;
                    customerChunkParameters.StartIndex = customerChunkParameters.ChunkSize * i;
                    ThreadPool.QueueUserWorkItem(LoadChunk, customerChunkParameters);
                }
                _countdownEvent.Wait();
            }
            sw.Stop();
            Console.WriteLine($"Loaded data in {sw.ElapsedMilliseconds} ms");
        }


        private void LoadChunk(object obj)
        {
            var customerChunkParameters = (CustomerChunkParameters)obj;
            for (int i = customerChunkParameters.StartIndex; i < customerChunkParameters.StartIndex + customerChunkParameters.ChunkSize; i++)
            {
                if (!Retrier.Retry(() => _repository.AddCustomerAsync(_customers[i]), _maxNumberOfRetries))
                    break;
            }
            _countdownEvent.Signal();
        }


        class CustomerChunkParameters
        {
            public int StartIndex { get; set; }
            public int ChunkSize { get; set; }
        }
    }
}
