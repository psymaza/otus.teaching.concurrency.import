﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using Microsoft.Extensions.Configuration;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Loader.Loaders;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        private const string HandlerProcessFileName = "Otus.Teaching.Concurrency.Import.DataGenerator.App.exe";
        private static string _handlerProcessDirectory;
     
        static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
.SetBasePath(Directory.GetCurrentDirectory())
.AddJsonFile("appSettings.json");
            var configuration = builder.Build();

            if (!int.TryParse(configuration["startType"], out int startType))
            {
                Console.WriteLine($"Указан некорректный параметр в конфиг файле appsettings.json {configuration["startType"]}");
            }
            string fileName = configuration["fileName"];
            if (!int.TryParse(configuration["numberOfRecords"], out int numberOfRecords))
            {
                Console.WriteLine($"Указан некорректный параметр в конфиг файле appsettings.json {configuration["numberOfRecords"]}");
            }
            _handlerProcessDirectory = configuration["processFolder"];

            switch (startType)
            {
                case 0:
                    {
                        var process = StartHandlerProcess(fileName, numberOfRecords);
                        Console.WriteLine($"Loader started with process Id {process.Id}...");
                    }
                    break;
                case 1:
                    {
                        Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");
                        GenerateCustomersDataFile(fileName, numberOfRecords);
                        var parser = new XmlParser(fileName);
                        var customers = parser.Parse();
                        var loader = new ThreadPoolDataLoader(customers, 10, new CustomerRepository(), 100);
                        loader.LoadData();
                    }
                    break;
                default:
                    Console.WriteLine($"Указан некорректный параметр в конфиг файле appsettings.json {configuration["startType"]}");
                    break;

            }
        }

        static void GenerateCustomersDataFile(string fileName, int numberOfRecords)
        {
            var xmlGenerator = new XmlGenerator(fileName, numberOfRecords);
            xmlGenerator.Generate();
        }
        private static Process StartHandlerProcess(string fileName, int numberOfRecords)
        {
            var startInfo = new ProcessStartInfo()
            {
                ArgumentList = { fileName, numberOfRecords.ToString() },
                FileName = GetPathToHandlerProcess(),
            };

            var process = Process.Start(startInfo);

            return process;
        }

        private static string GetPathToHandlerProcess()
        {
            return Path.Combine(_handlerProcessDirectory, HandlerProcessFileName);
        }
    }
}